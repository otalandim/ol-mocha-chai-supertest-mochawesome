const Joi = require('joi')

const schemaLogin = Joi.object({
    token: Joi.string()
})

module.exports = {
    schemaLogin
}