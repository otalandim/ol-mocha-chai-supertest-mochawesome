const Joi = require('joi');

const schemaData = Joi.object({
    id: Joi.number(),
    email: Joi.string(),
    first_name: Joi.string(),
    last_name: Joi.string(),
    avatar: Joi.string()
})

const schemaAd = Joi.object({
    company: Joi.string(),
    url: Joi.string(),
    text: Joi.string()
})

const schemaUserFull = Joi.object({
    data: schemaData,
    ad: schemaAd
}).unknown()

module.exports = {
    schemaUserFull
}
