const request = require('supertest')
const { expect } = require('chai')

const URL = 'https://reqres.in';
const pageNumber = 2
const userIdExist = 2
const userIdNotFound = 23231

describe('/GET Usuários', () => {
    it('deve retornar a lista de usuários', (done) => {
        request(URL)
            .get(`/api/users?page=${pageNumber}`)            
            .expect('Content-Type', /json/)
            .end((err, res) => {
                expect(res.statusCode).to.be.eq(200)
                expect(res.body.data).to.be.have.length(6);
                done()
            })
    })

    it('deve retornar usuário especifico', (done) => {
        request(URL)
        .get(`/api/users/${userIdExist}`)            
        .expect('Content-Type', /json/)
        .end((err, res) => {
            expect(res.statusCode).to.be.eq(200)
            expect(res.body.data.first_name).to.be.eq('Janet')
            done()
        })
    })

    it('deve retornar usuário não encontrado', (done) => {
        request(URL)
            .get(`/api/users/${userIdNotFound}`)            
            .expect('Content-Type', /json/)
            .end((err, res) => {
                expect(res.statusCode).to.be.eq(404)
                expect(res.body).to.be.empty                
                done()
            })
    })
})