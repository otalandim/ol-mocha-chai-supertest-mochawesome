const request = require('supertest')
const { expect } = require('chai')

const URL = 'https://reqres.in';
const { bodyEmailAndPassword, bodyEmail } = require('../bodys/bodys')

describe('/POST Registrar', () => {
    it('deve efetuar login com sucesso', (done) => {
       request(URL)
            .post('/api/login')
            .send(bodyEmailAndPassword)
            .end((err, res) => {
                expect(res.status).to.be.eq(200)
                expect(res.body).to.be.haveOwnProperty('token')
                done()
            })     
    })

    it('não deve registrar', (done) => {
        request(URL)
            .post('/api/login')
            .send(bodyEmail)
            .end((err, res) => {
                expect(res.status).to.be.eq(400)
                expect(res.body).to.be.haveOwnProperty('error')
                expect(res.body.error).to.be.eq('Missing password')
                done()
        })  
    })
})