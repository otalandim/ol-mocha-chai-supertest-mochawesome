const Joi = require('Joi')
const request = require('supertest')
const { expect } = require('chai')
const joiAssert = require('joi-assert')

const URL = 'https://reqres.in';
const userIdExist = 2;
const { schemaUserFull } = require('../schemas/schema.users')

describe.only('/GET Registrar', () => {
    it('deve validar o contrato enviados no body', (done) => {
        request(URL)
            .get(`/api/users/${userIdExist}`)
            .end((err, res) => {
                console.log(res.body);
                
                expect(res.status).to.be.eq(200)
                joiAssert(res.body, schemaUserFull)
                done()
            })   
        })
})