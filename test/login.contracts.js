const Joi = require('Joi')
const request = require('supertest')
const { expect } = require('chai')
const joiAssert = require('joi-assert')

const URL = 'https://reqres.in';
const { bodyEmailAndPassword, bodyEmail } = require('../bodys/bodys')
const { schemaLogin } = require('../schemas/schema.login')

describe('/POST Registrar', () => {
    it('deve validar o contrato enviados no body', (done) => {
        request(URL)
            .post('/api/login')
            .send(bodyEmailAndPassword)
            .end((err, res) => {
                expect(res.status).to.be.eq(200)
                joiAssert(res.body, schemaLogin)
                done()
            })   
        })
})